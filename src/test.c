#include "test.h"
#include "mem.h"
#include "mem_internals.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

int test1() 
{
    size_t heapSize = sysconf(_SC_PAGESIZE) * 8;
    size_t test_size = 1024;
    void* heap = heap_init(heapSize);
    void* block = _malloc(test_size);
    struct block_header* header = block_get_header(block);

    puts("Normal successful memory allocation\n");

    if (heap == NULL || block == NULL || header -> is_free || header -> capacity.bytes != test_size)
    {
        puts("Test 1 failed. Cannot initialize heap...\n");
        return EXIT_FAILURE;
    }

    _free(block);
    puts("Test 1 passed...\n");
    munmap(heap, heapSize);

    return EXIT_SUCCESS;
}

int test2()
{
    size_t heapSize = sysconf(_SC_PAGESIZE) * 8;
    void* heap = heap_init(heapSize);
    void* block1 = _malloc(1003);
    void* block2 = _malloc(3000);

    struct block_header* addr_block1 = block_get_header(block1);
    struct block_header* addr_block2 = block_get_header(block2);

    puts("Freeing one block from several allocated\n");

    if (!block1 || !block2)
    {
       puts("Test 2 failed. Bip bup bip...\n");
       return EXIT_FAILURE; 
    }

    if (addr_block1->capacity.bytes != 1003 || addr_block2->capacity.bytes != 3000) 
    {
       puts("Test 2 failed. Bip bup bip...\n");
       return EXIT_FAILURE; 
    }

    _free(block1);

    if (!addr_block1 -> is_free || addr_block2 -> is_free) 
    {
       puts("Test 2 failed. Bip bup bip...\n");
       return EXIT_FAILURE; 
    }

    _free(block2);
    puts("Test 2 passed...\n");
    munmap(heap, heapSize);

    return EXIT_SUCCESS;
}

int test3()
{   
    size_t heapSize = sysconf(_SC_PAGESIZE) * 8;
    void* heap = heap_init(heapSize);
    void* block1 = _malloc(1010);
    void* block2 = _malloc(2020);
    void* block3 = _malloc(3030);

    puts("Freeing two blocks from multiple allocations\n");

    if (block1 == NULL || block2 == NULL || block3 == NULL)
    {
        puts("Test 3 failed. Bip bup bip...\n");
        return EXIT_FAILURE;
    }

    struct block_header* addr_block1=block_get_header(block1);
    struct block_header* addr_block2=block_get_header(block2);
    struct block_header* addr_block3=block_get_header(block3);

    if(addr_block1 -> capacity.bytes != 1010 || addr_block2 -> capacity.bytes != 2020 || addr_block3 -> capacity.bytes != 3030)
    {
        puts("Test 3 failed. Bip bup bip...\n");
        return EXIT_FAILURE;
    }

    _free(block1);
    _free(block2);

    if (!(addr_block1 -> is_free && addr_block2 -> is_free) || addr_block3 -> is_free)
    {
        puts("Test 3 failed. Bip bup bip...\n");
        return EXIT_FAILURE;
    }

    puts("Test 3 passed...\n");
    munmap(heap, heapSize);
    
    return EXIT_SUCCESS;
}

int test4()
{   
    size_t heapSize = sysconf(_SC_PAGESIZE) * 8;
    void* heap = heap_init(heapSize);
    void* block = _malloc(REGION_MIN_SIZE + 1);
    struct block_header* addr_block = block_get_header(block);

    puts("The memory is over, the new memory region expands the old one\n");
    
    if (block == NULL)
    {
        puts("Test 4 failed. Bip bup bip...\n");
        return EXIT_FAILURE;
    }

    if(addr_block != block || addr_block -> capacity.bytes != heapSize*2)
    {
        puts("Test 4 failed. Bip bup bip...\n");
        return EXIT_FAILURE;
    }

    _free(block);
    puts("Test 4 passed...\n");
    munmap(heap, heapSize);

    return EXIT_SUCCESS;
}

int test5()
{   
    size_t heapSize = sysconf(_SC_PAGESIZE) * 8;
    void* heap = heap_init(heapSize);
    void* block = _malloc(REGION_MIN_SIZE + 1);

    puts("Out of memory, old memory region cannot be expanded due to different allocated address range, new region\n");

    (void) mmap(HEAP_START + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);


    if (block == NULL)
    {
        puts("Test 5 failed. Bip bup bip...\n");
        return EXIT_FAILURE;
    }

    struct block_header* addr_block = block_get_header(block);

    if(addr_block != block || addr_block -> capacity.bytes != 100 || addr_block -> is_free)
    {
        puts("Test 5 failed. Bip bup bip...\n");
        return EXIT_FAILURE;
    }

    _free(block);
    puts("Test 5 passed...\n");
    munmap(heap, heapSize);
    
    return EXIT_SUCCESS;
}

void test()
{
    if (test1() && test2() && test3() && test4() && test5())
    {
        puts("All test passed...\n");
    }
    else
    {
        puts("Something test wrong...");
    }
}



